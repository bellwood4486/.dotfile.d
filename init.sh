#!/bin/bash

readonly DOTFILE_DIR=$(cd $(dirname $0) && pwd)

if [[ "$OSTYPE" =~ cygwin ]];then
  if [[ ! $(echo $CYGWIN | grep "winsymlinks:native") ]]; then
    export CYGWIN="winsymlinks:native $CYGWIN"
  fi
fi

main()
{
  linkToHome $DOTFILE_DIR/.zshrc
  linkToHome $DOTFILE_DIR/.zshrc_cygwin
  linkToHome $DOTFILE_DIR/.zshrc_darwin
  linkToHome $DOTFILE_DIR/.zshrc_linux
  linkToHome $DOTFILE_DIR/.zshrc_msys
  linkToHome $DOTFILE_DIR/.bash_profile
  linkToHome $DOTFILE_DIR/.bashrc
  linkToHome $DOTFILE_DIR/.gitconfig
  linkToHome $DOTFILE_DIR/.gitignore_global
  linkToHome $DOTFILE_DIR/.gitattributes_global
  linkToHome $DOTFILE_DIR/.vimrc
  linkToHome $DOTFILE_DIR/.gvimrc
  if [[ "$OSTYPE" =~ cygwin ]];then
    linkToHome $DOTFILE_DIR/.minttyrc
    linkToHome $DOTFILE_DIR/.ck.config.js
    # 以下のリンクがなくてもKaoriya版VIMは動作してるので
    # とりあえずコメントアウト
    # gvim for windows can not read unix base symbolic lins.
    # create windows base one.
    #linkToHome .vimrc _vimrc
    #linkToHome .gvimrc _gvimrc
    #linkToHome .vim vimfiles -d
  fi
}

linkToHome()
{
  target=$1
  if [[ -n "$2" ]]; then
    linkname=$2
  else
    linkname=$(basename $target)
  fi
  option=$3
  pushd $HOME > /dev/null
  if [[ -L $linkname || -f $linkname ]]; then
    rm $linkname
  fi
  ln -sFv $option $target $linkname
  popd > /dev/null
}

main
