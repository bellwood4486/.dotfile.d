app.Env("CYGWIN") = "nodosfilewarning";

Config.tty.execute_command = "/bin/zsh -i";
Config.tty.title = "ck";
Config.tty.savelines = 10000;
Config.tty.input_encoding   = Encoding.UTF8;
Config.tty.display_encoding = Encoding.SJIS | Encoding.EUCJP | Encoding.UTF8;
Config.tty.scroll_key = 1;
Config.tty.scroll_output = 0;
Config.tty.bs_as_del  = 1;
Config.tty.use_bell   = 0;
Config.tty.cjk_width  = 1;

Config.window.position_x = null;
Config.window.position_y = null;
Config.window.cols = 100;
Config.window.rows = 36;

Config.window.scrollbar_show  = 1;
Config.window.scrollbar_right = 1;
Config.window.blink_cursor    = 1;
Config.window.zorder          = WinZOrder.Normal;
Config.window.transparent     = 0xFF;
Config.window.linespace       = 0;
Config.window.border_left     = 1;
Config.window.border_top      = 1;
Config.window.border_right    = 1;
Config.window.border_bottom   = 1;

Config.window.font_name = "Consolas";
Config.window.font_size = 9;

Config.window.background_file     = "";
Config.window.background_repeat_x = Place.NoRepeat;
Config.window.background_repeat_y = Place.NoRepeat;
Config.window.background_align_x  = Align.Center;
Config.window.background_align_y  = Align.Center;
Config.window.color_foreground    = 0xFFFFFF;
Config.window.color_background    = 0x000000;
Config.window.color_selection     = 0x663297FD;
Config.window.color_cursor        = 0x00AA00;
Config.window.color_imecursor     = 0xAA0000;

Config.window.color_alpha   = 0xCC;
Config.window.color_color0  = 0x606261; // Black
Config.window.color_color1  = 0xff8177; // Red
Config.window.color_color2  = 0xadfb85; // Green
Config.window.color_color3  = 0xfffdca; // Yellow
Config.window.color_color4  = 0xa5d6f9; // Blue
Config.window.color_color5  = 0xff90f7; // Magenta
Config.window.color_color6  = 0xa2feff; // Cyan
Config.window.color_color7  = 0xf0f0f0; // White
Config.window.color_color8  = 0x404040; // BoldBlack
Config.window.color_color9  = 0xff6400; // BoldRed
Config.window.color_color10 = 0xb7ea11; // BoldGreen
Config.window.color_color11 = 0xeace1c; // BoldYellow
Config.window.color_color12 = 0x6b9bdb; // BoldBlue
Config.window.color_color13 = 0xed9db9; // BoldMagenta
Config.window.color_color14 = 0x40ffff; // BoldCyan
Config.window.color_color15 = 0xffffff; // BoldWhite

Config.window.mouse_left         = MouseCmd.Select;
Config.window.mouse_middle       = MouseCmd.Paste;
Config.window.mouse_right        = MouseCmd.Menu;

Config.accelkey.new_shell        = Keys.ShiftL | Keys.CtrlL | Keys.N;
Config.accelkey.new_window       = Keys.ShiftL | Keys.CtrlL | Keys.M;
Config.accelkey.open_window      = Keys.ShiftL | Keys.CtrlL | Keys.O;
Config.accelkey.close_window     = Keys.ShiftL | Keys.CtrlL | Keys.W;
Config.accelkey.next_shell       = Keys.CtrlL  | Keys.Tab;
Config.accelkey.prev_shell       = Keys.ShiftL | Keys.CtrlL | Keys.Tab;
Config.accelkey.paste            = Keys.ShiftL | Keys.Insert;
Config.accelkey.popup_menu       = Keys.ShiftL | Keys.F10;
Config.accelkey.popup_sys_menu   = Keys.AltR   | Keys.Space;
Config.accelkey.scroll_page_up   = Keys.ShiftL | Keys.PageUp;
Config.accelkey.scroll_page_down = Keys.ShiftL | Keys.PageDown;
Config.accelkey.scroll_line_up   = Keys.None;
Config.accelkey.scroll_line_down = Keys.None;
Config.accelkey.scroll_top       = Keys.ShiftL | Keys.Home;
Config.accelkey.scroll_bottom    = Keys.ShiftL | Keys.End;
