cygwin=false;
darwin=false;
case "$(uname)" in
    CYGWIN*) cygwin=true ;;
    Darwin*) darwin=true ;;
esac

# Completion
# if $darwin; then
#     if [ -f $(brew --prefix)/etc/bash_completion ]; then
# 	. $(brew --prefix)/etc/bash_completion
#     fi
#     git_ps1_file=/usr/local/etc/bash_completion.d/git-prompt.sh
# elif $cygwin; then
#     if [ -f /etc/bash_completion ]; then
# 	. /etc/bash_completion
#     fi
#     git_ps1_file=/etc/bash_completion.d/git
# fi

# Environments
# if [ -f $git_ps1_file ]; then
#     export PS1='\[\e]0;\w\a\]\[\e[32m\]\u@\h\[\e[00m\]:\[\e[34m\]\w\[\e[31m\]$(__git_ps1)\[\e[00m\]\n\$ '
# else
#     export PS1='\[\e]0;\w\a\]\[\e[32m\]\u@\h\[\e[00m\]:\[\e[34m\]\w\[\e[00m\]\n\$ '
# fi
if [ -f $(which vim) ]; then
    export EDITOR=vim
else
    export EDITOR=vi
fi
if [ -d "~/bin" ]; then
    export PATH=~/bin:$PATH
fi
# Ignore some controlling instructions
# HISTIGNORE is a colon-delimited list of patterns which should be excluded.
# The '&' is a special pattern which suppresses duplicate entries.
export HISTIGNORE=$'[ \t]*:&:[fb]g:exit'

# Load .bashrc & .bashrc_local
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi
if [ -f ~/.bashrc_local ]; then
    . ~/.bashrc_local
fi

