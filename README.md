# 各種設定ファイル

以下のツールの設定ファイルを管理するリポジトリです。

- zsh
- bash
- ck
- mintty
- git
- emacs

## ファイルの配置方法

```
% git clone git@bitbucket.org:bellwood4486/.dotfile.d.git
% cd .dotfile.d
% ./init.sh
```

## gitで改行コードを扱うときの方針

- .gitconfigのcore.autocrlfは常にfalseにし、この設定では改行コードに関する設定はしない。
  - trueにすると、.vimrcや*.vimファイルがエラーで読み込みない。(vim関連の設定ファイルは、改行コードがLFである必要あり)
  - inputにすると、*.slnファイルがエラー読めない。(VisualStudio関連の一部は改行コードがCRLFである必要あり)
- チェックイン時のノーマライズおよびチェックアウト時のコンバートは、.gitattributesで設定する。

参考

https://www.kernel.org/pub/software/scm/git/docs/gitattributes.html

https://help.github.com/articles/dealing-with-line-endings
